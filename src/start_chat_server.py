from server.app import ChatServer

CSELECTED = '\33[7m'
CEND = '\33[0m'


def main():
    print("Création d'un nouveau serveur de chat")
    server_name = input(CSELECTED + "Donnez un joli nom au serveur : " + CEND)
    ChatServer(server_name).start()


if __name__ == '__main__':
    main()
