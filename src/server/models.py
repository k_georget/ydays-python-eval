import json
import random

USER_COLORS = ['\33[91m', '\33[92m', '\33[93m', '\33[94m', '\33[96m']


class User:
    def __init__(self, name, socket):
        self.name = name
        self.socket = socket
        self.color = random.choice(USER_COLORS)


class Message:
    def __init__(self, msg_type, content, sender='unknown', recipient='unknown'):
        self.type = msg_type
        self.content = content
        self.sender = sender
        self.recipient = recipient

    def to_encoded_json(self):
        return json.dumps(self.__dict__).encode('utf-8')
