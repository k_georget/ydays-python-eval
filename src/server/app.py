import socket
import json
from threading import Thread
from .models import Message, User


class ChatServer:
    def __init__(self, name="ChatServer", max_users=10, host="127.0.0.1", port=8080):
        self.name = name
        self.max_users = max_users
        self.sock = socket.socket()
        self.host = host
        self.port = port
        self.users = []

    def start(self):
        self.sock.bind((self.host, self.port))
        self.sock.listen(self.max_users)
        print('Serveur démarré !')
        print("En attente d'utilisateurs")
        while True:
            client, address = self.sock.accept()
            thread = Thread(target=self.on_new_client, args=(client, address))
            thread.start()
        self.sock.close()

    def on_new_user(self, msg, client_socket):
        user_name = msg["sender"]
        self.users.append(User(user_name, client_socket))
        msg_content = "Bienvenu sur " + self.name + ", " + user_name + " !!"
        response = Message("text", msg_content, "ChatBot", user_name)
        client_socket.send(response.to_encoded_json())

    def message_handler(self, msg, client_socket):
        msg = json.loads(msg.decode('utf-8'))
        if msg["type"] == 'user_introduction':
            self.on_new_user(msg, client_socket)
        else:
            content = msg['content']
            sender = msg['sender']
            recipient = msg['recipient']

            if recipient == 'ALL':
                for user in self.users:
                    message_inst = Message('text', content, sender, user.name)
                    try:
                        user.socket.send(message_inst.to_encoded_json())
                    except:
                        # TODO Delete user from server's user list when disconnected :)
                        print('ERROR DUE TO NON PURGED USER IN LIST, THIS IS A TODO')
            else:
                recipient_user = next(filter(lambda user: user.name == recipient, self.users), None)

                if recipient_user is not None:
                    print(recipient_user.name)
                    message_inst = Message('text', content, sender, recipient)
                    recipient_user.socket.send(message_inst.to_encoded_json())

    def on_new_client(self, client_socket, address):
        while True:
            client_msg = client_socket.recv(255)
            if not client_msg:
                break

            self.message_handler(client_msg, client_socket)

        print('ONE USER HAVE LEFT')
        client_socket.close()
