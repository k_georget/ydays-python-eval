import socket
import json
from threading import Thread

# PRINT COLORS WRAPPERS
CBLINK = '\33[5m'
CEND = '\33[0m'
CSELECTED = '\33[7m'
CYELLOW2 = '\33[93m'
CVIOLET2 = '\33[95m'


class User:
    def __init__(self, name):
        self.name = name


class Message:
    def __init__(self, msg_type, content, sender='unknown', recipient='unknown'):
        self.type = msg_type
        self.content = content
        self.sender = sender
        self.recipient = recipient

    def to_encoded_json(self):
        return json.dumps(self.__dict__).encode('utf-8')


class Client:
    def __init__(self, user: User, server_ip='127.0.0.1', server_port=8080):
        self.user = user
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_ip = server_ip
        self.server_port = server_port

    def new_message_listener(self):
        while True:
            message = self.sock.recv(255)
            data = json.loads(message.decode('utf-8'))
            print(CVIOLET2 + data['sender'] + " vous dit : " + CEND)
            print(CYELLOW2 + data['content'] + CEND)

    def start_chat(self):
        self.sock.connect((self.server_ip, self.server_port))
        user_introduction_message = Message('user_introduction', '', self.user.name)
        self.sock.send(user_introduction_message.to_encoded_json())

        listen_thread = Thread(target=self.new_message_listener)
        listen_thread.start()

        while True:
            recipient = input("\n" + CSELECTED + 'A qui envoyer un msg ?' + CEND + "\n")
            msg = input(CBLINK + '>>> ' + CBLINK)
            message_inst = Message('text', msg, self.user.name, recipient)
            self.sock.send(message_inst.to_encoded_json())
