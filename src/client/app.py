from .models import Client, User

CSELECTED = '\33[7m'
CEND = '\33[0m'

def run():
    name = input(CSELECTED + 'Quel est ton nom ?' + CEND)
    user = User(name)
    client = Client(user)
    client.start_chat()
