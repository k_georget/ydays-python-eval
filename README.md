# Evaluation Python - Realtime Chat CLI

## Environnement
Python 3.8.7, et c'est tout :)

## Utiliser le chat 
### Lancement du serveur chat
Ouvrir un terminal, naviguer dans le dossier src du projet et lancer un serveur de chat local.    
`cd src`  
`python3 start_chat_server.py`

Renseignez ensuite un nom pour votre serveur.


### Lancement d'un client 
Ouvrir une autre instance de terminal et lancer un client connecté au serveur de chat  
`cd src`  
`python3 start_client_server.py`  

#### Utilisation du chat
* Votre nom vous est demandé à la connexion, il s'agit de votre identifiant et il doit être unique sur le serveur. 
* Lancez un deuxième client et renseignez un autre nom.
* Vous pouvez ensuite envoyer des messages à la personne de votre choix en renseignant d'abord son nom (identifiant), puis le contenu du message.
* SI vous voulez envoyer un message à tous les membres du groupe, renseignez la valeur *ALL*
* Le serveur peut accueillir jusqu'à 10 utilisateurs en temps réel :)


### TO BE CONTINUED ...
Gestion de multi serveurs chat, interface web, passage sur Flask ou Django avec socket.io, containerisation.
