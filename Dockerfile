FROM python:3.8.7-alpine

# set work directory
WORKDIR /usr/src/app

# set environment variables

# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt /usr/src/app/requirements.txt
RUN pip install -r requirements.txt

# copy project
COPY src /usr/src/app/

ENV FLASK_APP=app.py
ENV FLASK_ENV=development
CMD flask run --host=0.0.0.0
